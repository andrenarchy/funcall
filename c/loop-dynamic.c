/*----------------------------------------------------------------------------
  Copyright (C) 2012 André Gaul <gaul@math.tu-berlin.de>
  Licensed under the GNU GPLv3 , http://www.gnu.org/licenses/gpl-3.0.html
----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include "fun.h"

double* loop(int n) {
    double* result = (double*) malloc(sizeof(double)*n);
    for (int i=0; i<n; i++)
        result[i] = fun(i);
    free(result);
    return result;
}

/* Copied from
   http://www.gnu.org/software/libc/manual/html_node/Elapsed-Time.html

   Subtract the `struct timeval' values X and Y,
   storing the result in RESULT.
   Return 1 if the difference is negative, otherwise 0.  */
int timeval_subtract (struct timeval *result, struct timeval *x, 
        struct timeval *y) {
    /* Perform the carry for the later subtraction by updating y. */
    if (x->tv_usec < y->tv_usec) {
        int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
        y->tv_usec -= 1000000 * nsec;
        y->tv_sec += nsec;
    }
    if (x->tv_usec - y->tv_usec > 1000000) {
        int nsec = (x->tv_usec - y->tv_usec) / 1000000;
        y->tv_usec += 1000000 * nsec;
        y->tv_sec -= nsec;
    }

    /* Compute the time remaining to wait.
       tv_usec is certainly positive. */
    result->tv_sec = x->tv_sec - y->tv_sec;
    result->tv_usec = x->tv_usec - y->tv_usec;

    /* Return 1 if result is negative. */
    return x->tv_sec < y->tv_sec;
}

int main(int argc, char **args) {
    int n = 10000000;
    int runs = 10;

    struct timeval start, stop, diff;
    gettimeofday(&start, NULL);
    for (int i=0; i<runs; i++)
        loop(n);
    gettimeofday(&stop, NULL);
    timeval_subtract(&diff, &stop, &start);
    double funtime = ((double)diff.tv_sec + diff.tv_usec/1e6)/runs;
    printf("Function \"loop\" took %f seconds (average of %d runs).\n",
           funtime, runs);
    
    return 0;
}


