/*----------------------------------------------------------------------------
  Copyright (C) 2012 André Gaul <gaul@math.tu-berlin.de>
  Licensed under the GNU GPLv3 , http://www.gnu.org/licenses/gpl-3.0.html
----------------------------------------------------------------------------*/

double fun(double a);
