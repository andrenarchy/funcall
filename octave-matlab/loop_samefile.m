% ----------------------------------------------------------------------------
% Copyright (C) 2012 André Gaul <gaul@math.tu-berlin.de>
% Licensed under the GNU GPLv3 , http://www.gnu.org/licenses/gpl-3.0.html
% ----------------------------------------------------------------------------
function result = loop_samefile(n)
    result = zeros(n,1);
    for i=1:n
        result(i) = fun_samefile(i);
    end
end

function result = fun_samefile(a)
    result = a*a;
end
