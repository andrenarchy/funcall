% ----------------------------------------------------------------------------
% Copyright (C) 2012 André Gaul <gaul@math.tu-berlin.de>
% Licensed under the GNU GPLv3 , http://www.gnu.org/licenses/gpl-3.0.html
% ----------------------------------------------------------------------------
function benchmark()
    funs = {@loop_external, 
        @loop_samefile, 
        @loop_anonymous, 
        @loop_nested
    };
    n = 10000000;
    runs = 10;
    for i=1:length(funs)
        tic;
        for j=1:runs
            funs{i}(n);
        end
        funtime = toc / runs;
        fprintf('Function "%s" took %g seconds (average of %d runs).\n', func2str(funs{i}), funtime, runs);
    end
end
