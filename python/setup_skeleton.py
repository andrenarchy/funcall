# -*- coding: utf-8 -*-
'''---------------------------------------------------------------------------
  Copyright (C) 2012 André Gaul <gaul@math.tu-berlin.de>
  Licensed under the GNU GPLv3 , http://www.gnu.org/licenses/gpl-3.0.html
---------------------------------------------------------------------------'''
# compile me with:
#   python setup_PYXNAME.py build_ext --inplace

from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [Extension("PYXNAME", ["PYXNAME.pyx"])]

setup(
  name = 'PYXNAME',
  cmdclass = {'build_ext': build_ext},
  ext_modules = ext_modules
)
