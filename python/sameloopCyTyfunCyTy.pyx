# -*- coding: utf-8 -*-
'''---------------------------------------------------------------------------
  Copyright (C) 2012 André Gaul <gaul@math.tu-berlin.de>
  Licensed under the GNU GPLv3 , http://www.gnu.org/licenses/gpl-3.0.html
---------------------------------------------------------------------------'''
cimport numpy
import numpy

cpdef double funCyTy(double a):
    return a**2

def sameloopCyTyfunCyTy(n):
    cdef numpy.ndarray[numpy.double_t] result = \
            numpy.empty(n)
    cdef int i
    for i in range(0,n):
        result[i] = funCyTy(i+1)
    return result
