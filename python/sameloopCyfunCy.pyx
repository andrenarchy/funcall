# -*- coding: utf-8 -*-
'''---------------------------------------------------------------------------
  Copyright (C) 2012 André Gaul <gaul@math.tu-berlin.de>
  Licensed under the GNU GPLv3 , http://www.gnu.org/licenses/gpl-3.0.html
---------------------------------------------------------------------------'''
cimport numpy
import numpy

def funCy(a):
    return a**2

def sameloopCyfunCy(n):
    result = numpy.empty(n)
    for i in range(0,n):
        result[i] = funCy(i+1)
    return result
