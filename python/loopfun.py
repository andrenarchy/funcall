# -*- coding: utf-8 -*-
'''---------------------------------------------------------------------------
  Copyright (C) 2012 André Gaul <gaul@math.tu-berlin.de>
  Licensed under the GNU GPLv3 , http://www.gnu.org/licenses/gpl-3.0.html
---------------------------------------------------------------------------'''
import fun
import numpy

def loopfun(n):
    result = numpy.empty(n)
    for i in range(0,n):
        result[i] = fun.fun(i+1)
    return result
