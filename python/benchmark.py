# -*- coding: utf-8 -*-
'''---------------------------------------------------------------------------
  Copyright (C) 2012 André Gaul <gaul@math.tu-berlin.de>
  Licensed under the GNU GPLv3 , http://www.gnu.org/licenses/gpl-3.0.html
---------------------------------------------------------------------------'''
# don't forget to run 'make'!

import glob
import time

funnames=[]
[funnames.extend(glob.glob(pat)) 
        for pat in ['loop*.py', 'loop*.pyx', 'same*.py', 'same*.pyx']]
funnames = [name.replace('.pyx','') for name in funnames]
funnames = [name.replace('.py','') for name in funnames]

n = 10000000
runs = 10
for name in funnames:
    fun = getattr(__import__(name), name)
    funtime = 0.0;
    start = time.time()
    for j in xrange(0,runs):
        result = fun(n)
    funtime = (time.time() - start)/runs
    print '%f seconds for function "%s" (average of %d runs).' % (funtime, name, runs)
